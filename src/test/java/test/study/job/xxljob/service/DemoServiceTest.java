package test.study.job.xxljob.service;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import study.job.xxljob.support.SpringBootCfg;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootCfg.class)
public class DemoServiceTest {
}
