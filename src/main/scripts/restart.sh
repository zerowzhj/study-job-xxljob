#!/bin/bash

DEPLOY_DIR=/xdfapp/study-job-xxljob
JAR_NAME=study-job-xxljob-1.0.jar

pid=$(ps -ef | grep $JAR_NAME | grep -v grep | awk '{print $2}')
echo "[INFO] pid= [$pid]"
if [ ! -n "$pid" ]; then
  echo "[WARN] No Job Service Running"
else
  echo "[INFO] Stop Job Service: $pid"
  sudo kill -9 $pid
  sleep 2
fi

source /etc/profile
nohup java -jar $DEPLOY_DIR/$JAR_NAME &
echo "[INFO] Job Service Started"
exit
