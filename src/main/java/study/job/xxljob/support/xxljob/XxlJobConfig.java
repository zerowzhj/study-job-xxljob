package study.job.xxljob.support.xxljob;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class XxlJobConfig {

    @Bean
    @ConfigurationProperties(prefix = "xxl.job")
    public XxlJobProperties xxlJobProperties() {
        return new XxlJobProperties();
    }

    @Bean
    public XxlJobSpringExecutor xxlJobSpringExecutor(XxlJobProperties props) {
        XxlJobSpringExecutor springExecutor = new XxlJobSpringExecutor();

        //调度中心
        springExecutor.setAdminAddresses(props.getAdmin().getAddresses());
        //执行器
        springExecutor.setAppname(props.getExecutor().getAppName());
        springExecutor.setIp(props.getExecutor().getIp());
        springExecutor.setPort(props.getExecutor().getPort());
        springExecutor.setLogPath(props.getExecutor().getLogPath());
        springExecutor.setLogRetentionDays(props.getExecutor().getLogRetentionDays());
        //token
        springExecutor.setAccessToken(props.getAccessToken());

        return springExecutor;
    }
}
