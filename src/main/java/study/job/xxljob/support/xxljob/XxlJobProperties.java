package study.job.xxljob.support.xxljob;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class XxlJobProperties {

    private Admin admin;
    private Executor executor;
    private String accessToken;

    @Data
    static class Admin {
        private String addresses;
    }

    @Data
    static class Executor {
        private String appName;
        private String ip;
        private int port;
        private String logPath;
        private int logRetentionDays;
    }
}
