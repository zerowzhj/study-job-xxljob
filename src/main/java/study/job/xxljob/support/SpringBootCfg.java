package study.job.xxljob.support;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "study.job.xxljob")
public class SpringBootCfg {

}
