package study.job.xxljob.job._2_routing;//package study.job.xxljob.job._2_routing;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * （★）路由策略
 * （1）第一个
 * （2）最后一个
 * （3）随机
 * （4）轮询
 */
@Slf4j
@Component
public class _1_RoutingJob {

    @XxlJob("routingJob")
    public ReturnT<String> execute(String param) throws Exception {
        log.info(">>>> i am _2_routing job");
//        if (1 == 1)
//            throw new RuntimeException("我是一个异常");
        return ReturnT.SUCCESS;
    }
}
