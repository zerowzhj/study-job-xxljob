package study.job.xxljob.job._2_routing;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 分片广播，以执行器为维度进行分片
 * （1）支持动态扩容执行器集群从而动态增加分片数量，协同进行业务处理
 * （2）在进行大数据量业务操作时可显著提升任务处理能力和速度
 */
@Slf4j
@Component
public class _2_ShardingJob {

    @XxlJob("shardingJob")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            // 获取分片信息
            int total = XxlJobHelper.getShardTotal();
            int index = XxlJobHelper.getShardIndex();
            log.info(">>>>>> index={}, total={}", index, total);

            // 根据分片信息查询分片数据, 利用函数 mod(id, total) = index

            return ReturnT.SUCCESS;
        } catch (Exception ex) {
            return ReturnT.FAIL;
        }
    }

    public static void main(String[] args) {
        int total = 5;
        // 取余或取模
        for (int i = 1; i <= 15; i++) {
            log.info("{} % {}= {}", i, total, i % total);
        }
    }
}
