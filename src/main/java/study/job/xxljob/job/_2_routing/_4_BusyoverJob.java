package study.job.xxljob.job._2_routing;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 忙碌转移
 */
@Slf4j
@Component
public class _4_BusyoverJob {

    @XxlJob("busyoverJob")
    public ReturnT<String> execute(String param) throws Exception {

        return ReturnT.SUCCESS;
    }
}
