package study.job.xxljob.job._2_routing;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 故障转移
 * （1）如果执行器集群中某一台机器故障，将会自动Failover切换到一台正常的执行器发送调度请求
 * （2）按照顺序依次进行心跳检测，第一个心跳检测成功的机器选定为目标执行器并发起调度
 */
@Slf4j
@Component
public class _3_FailoverJob {

    @XxlJob("failoverJob")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            log.info("i am failover job");
            if (1 == 1) {
                throw new RuntimeException("fssssssssssssss");
            }
            return ReturnT.SUCCESS;
        } catch (Exception ex) {
            return ReturnT.FAIL;
        }
    }
}
