package study.job.xxljob.job._4_;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 重试调度
 */
@Slf4j
@Component
public class _2_RetryJob {

    @XxlJob("retryJob")
    public ReturnT<String> execute(String param) throws Exception {
            log.info(">>>>>> retry start");
            TimeUnit.SECONDS.sleep(10);
            log.info(">>>>>> retry finish");

        return ReturnT.SUCCESS;
    }
}
