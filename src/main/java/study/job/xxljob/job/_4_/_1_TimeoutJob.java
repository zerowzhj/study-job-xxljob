package study.job.xxljob.job._4_;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;


/**
 * （★）超时
 * （1）触发时间：配置
 * （2）超时时间：规定
 * （3）执行时间：实际
 */
@Slf4j
@Component
public class _1_TimeoutJob {

    @XxlJob("timeoutJob")
    public ReturnT<String> execute(String param) throws Exception {

            log.info(">>>>>> timeout start");
            TimeUnit.SECONDS.sleep(7);
            log.info(">>>>>> timeout finish");

        return ReturnT.SUCCESS;
    }
}
