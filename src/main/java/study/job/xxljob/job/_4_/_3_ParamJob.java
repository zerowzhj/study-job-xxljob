package study.job.xxljob.job._4_;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 任务参数
 */
@Slf4j
@Component
public class _3_ParamJob {

    @XxlJob("paramJob")
    public ReturnT<String> execute(String param) throws Exception {
  log.info(">>>>>>>>>> {}", param);
        return ReturnT.SUCCESS;
    }
}
