/*
package study.job.xxljob.job._1_handler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.handler.annotation.JobHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@JobHandler(value = "jobHandler")
public class _1_JobHandler extends IJobHandler {

    @Override
    public void init() {
        log.info("init...");
    }

    @Override
    public void destroy() {
        log.info("destroy...");
    }

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        try {
            ReturnT<String> returnT = new ReturnT<>();
            returnT.setCode(ReturnT.SUCCESS_CODE);
            //执行备注
            returnT.setMsg("消息 消息 消息");
            returnT.setContent("内容 内容 内容");
            return returnT;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
*/
