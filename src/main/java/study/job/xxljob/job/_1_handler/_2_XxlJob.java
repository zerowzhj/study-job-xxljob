package study.job.xxljob.job._1_handler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class _2_XxlJob {

    /**
     * init 和 destroy 方法必须为 public
     */
    @XxlJob(value = "xxlJob", init = "init", destroy = "destroy")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            ReturnT<String> returnT = new ReturnT<>();
            returnT.setCode(ReturnT.SUCCESS_CODE);
            //执行备注
            returnT.setMsg("消息 消息 消息");
            returnT.setContent("内容 内容 内容");
            return returnT;
        } catch (Exception ex) {
            return ReturnT.FAIL;
        }
    }

    public void init() {
        log.info(">>>>>> init");
    }

    public void destroy() {
        log.info(">>>>>> destroy");
    }
}
