package study.job.xxljob.job._3_blocking;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 丢弃后续调度
 */
@Slf4j
@Component
public class _2_DiscardLaterJob {

    @XxlJob("discardFollowJob")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            log.info(">>>>>> discard follow job start");
            TimeUnit.SECONDS.sleep(10);
            log.info(">>>>>> discard follow  job finish");
            return ReturnT.SUCCESS;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
