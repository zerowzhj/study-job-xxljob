package study.job.xxljob.job._3_blocking;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 单机串行
 */
@Slf4j
@Component
public class _1_SerialJob {

    @XxlJob("serialJob")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            log.info(">>>>>> serial job start");
            TimeUnit.SECONDS.sleep(10);
            log.info(">>>>>> serial job finish");
            return ReturnT.SUCCESS;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
